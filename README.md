# live-campus-restoration-notifier
A script for Google Apps Script to tweet the Live Campus restoration information.

## License
Copyright (c) 2017 cygnus_beta  
Licensed under the Apache License Version 2.0, see [LICENSE.md](LICENSE.md).

the [OAuth1 for Apps Script](https://github.com/googlesamples/apps-script-oauth1) library is used.  
**Note:** Modified by me.
