var CONSUMER_KEY = '*************************';
var CONSUMER_SECRET = '**************************************************';

/**
 * Authorizes and makes a request to the Twitter API.
 */
function run(msg) {
    var service = getService();
    if (service.hasAccess()) {
        var url = 'https://api.twitter.com/1.1/statuses/update.json';
        var payload = {
            status: msg
        };
        var response = service.fetch(url, {
            method: 'post',
            payload: payload
        });
        var result = JSON.parse(response.getContentText());
        Logger.log(JSON.stringify(result, null, 2));
    } else {
        var authorizationUrl = service.authorize();
        Logger.log('Open the following URL and re-run the script: %s',
            authorizationUrl);
    }
}

/**
 * Reset the authorization state, so that it can be re-tested.
 */
function reset() {
    var service = getService();
    service.reset();
}

/**
 * Configures the service.
 */
function getService() {
    return OAuth1.createService('Twitter')
        // Set the endpoint URLs.
        .setAccessTokenUrl('https://api.twitter.com/oauth/access_token')
        .setRequestTokenUrl('https://api.twitter.com/oauth/request_token')
        .setAuthorizationUrl('https://api.twitter.com/oauth/authorize')

        // Set the consumer key and secret.
        .setConsumerKey(CONSUMER_KEY)
        .setConsumerSecret(CONSUMER_SECRET)

        // Set the name of the callback function in the script referenced
        // above that should be invoked to complete the OAuth flow.
        .setCallbackFunction('authCallback')

        // Set the property store where authorized tokens should be persisted.
        .setPropertyStore(PropertiesService.getUserProperties());
}

/**
 * Handles the OAuth callback.
 */
function authCallback(request) {
    var service = getService();
    var authorized = service.handleCallback(request);
    if (authorized) {
        return HtmlService.createHtmlOutput('Success!');
    } else {
        return HtmlService.createHtmlOutput('Denied');
    }
}

/**
 * フォーマットされた日付と時刻を返す。
 * @param {?string} dateStr
 *     引数なし || 'now' -> 現在日時をフォーマットしてから返す。
 *     引数が'yyyyMMddhhmmss' -> 与えられた日時をフォーマットしてから返す。
 * @return {string} 'MM/dd[EEE] hh:mm'｜ただしEEEは大文字
 * @example
 * formattedTime();
 * // return '02/27[MON] 22:37' // 現在日時
 * formattedTime('20170228173704');
 * // return '02/28[TUE] 17:37' // 与えられた日時
 */
var formattedTime = function (dateStr) {
    try {
        if (!dateStr || dateStr === 'now') { // 引数なし || 引数が 'now' のとき
            var date = new Date(); // 現在日時を生成
        } else {
            /**
             * dateStr4newDate
             * new Date() に入れられるようにフォーマットされた日時
             * @type {string}
             * @example '2017/02/28 17:37'
             */
            var dateStr4newDate = dateStr.slice(0, 4) + '/' +
                dateStr.slice(4, 6) + '/' +
                dateStr.slice(6, 8) + ' ' +
                dateStr.slice(8, 10) + ':' +
                dateStr.slice(10, 12);

            var date = new Date(dateStr4newDate); // 与えられた日時を生成

            /** @type {boolean} */
            var isAnInvalidDate = date.toString() == "Invalid Date";

            if (isAnInvalidDate) throw new Error('dateStr is invalid.');
        }

        var obj = {
            MM: date.getMonth() + 1, // 月を取得（実際の月にするために+1する）
            dd: date.getDate(), // 日を取得
            hh: date.getHours(), // 時を取得
            mm: date.getMinutes() // 分を取得
        };

        // 月、日、時、分が一桁の場合は先頭に0をつける
        for (var ii in obj) {
            // obj[ii]には obj.MM、obj.dd、obj.hh、obj.mm が入る
            if (obj[ii] < 10) {
                obj[ii] = "0" + obj[ii].toString();
            } else {
                // 既に２桁だった場合は数値のままなので、念のため文字列に変換し
                // ておく。
                obj[ii] = obj[ii].toString();
            }
        }
        var EEENum = date.getDay(); // 曜日を取得（数値）

        // 曜日を数値から文字列に変換するための配列
        var week = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

        // 曜日を数値から文字列に変換
        var EEE = week[EEENum];

        // フォーマットを整える
        var result = obj.MM + '/' + obj.dd + '[' + EEE + '] ' + obj.hh + ':' +
            obj.mm;

        return result;
    } catch (e) {
        // デバッグ用｜エラーメッセージをそのままスクリプトプロパティに格納して
        // おく
        var errorKey = 'ERROR_' + formattedTime('now') + '_' + 'formattedTime';

        var errorValue = e.name + ': ' + 'formattedTime' + '() | line ' +
            e.lineNumber + ' | ' + e.message;

        PropertiesService.getScriptProperties().
            setProperty(errorKey, errorValue);
    }
};

function fetchLiveCampus() {
    var isTweetedValue =
        PropertiesService.getScriptProperties().getProperty(
            'isTweeted'
        );

    var isTweeted = isTweetedValue == 'true';

    if (isTweeted) return;

    var res = UrlFetchApp.fetch(
        'https://i-student.ibaraki.ac.jp',
        { muteHttpExceptions: true }
    );

    /** @type {boolean} */
    var wasFailureToFetch = res.getResponseCode() !== 200;

    if (wasFailureToFetch) return;

    var msg = [
        '■自動投稿',
        formattedTime(),
        ,
        'Live Campus が復帰しました'
    ].join('\n');

    run(msg);

    PropertiesService.getScriptProperties().
        setProperty(
            'isTweeted',
            'true'
        );
}

function test() {
    var msg = [
        '■自動投稿',
        formattedTime(),
        ,
        'てすと'
    ].join('\n');

    run(msg);
}
